﻿using System;
using System.Diagnostics;

namespace CMDUtility
{
    public class CMDUtility : IDisposable
    {
        public bool RedirectStandardInput;
        public bool RedirectStandardOutput;
        public bool CreateNoWindow;
        public bool UseShellExecute;
        private Process cmd;

        public CMDUtility()
        {
            this.RedirectStandardInput = true;
            this.RedirectStandardOutput = true;
            this.CreateNoWindow = true;
            this.UseShellExecute = false;
            this.cmd = new Process();

            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = RedirectStandardInput;
            cmd.StartInfo.RedirectStandardOutput = RedirectStandardOutput;
            cmd.StartInfo.CreateNoWindow = CreateNoWindow;
            cmd.StartInfo.UseShellExecute = UseShellExecute;
        }

        public string ExecuteCommand(string CommandString)
        {

            cmd.Start();

            cmd.StandardInput.WriteLine(CommandString);
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            
            string result = cmd.StandardOutput.ReadToEnd();
            return result;


        }

        /// <summary>
        /// Execute command line command in specified directory.
        /// </summary>
        /// <param name="WorkingDirectory"></param>
        /// <param name="CommandString"></param>
        /// <returns></returns>
        public string ExecuteCommand(string WorkingDirectory, string CommandString)
        {
            cmd.StartInfo.WorkingDirectory = WorkingDirectory;

            cmd.Start();

            cmd.StandardInput.WriteLine(CommandString);
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();

            string result = cmd.StandardOutput.ReadToEnd();
            return result;
        }

        public void Dispose()
        {
            if (cmd != null)
            {
                cmd.Dispose(); 
            }
        }
    }
}
