﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace XMLSerialization
{
    public class XMLSerialization
    {


        /// <summary>
        /// Zapisuje obiekt jako plik xml.
        /// </summary>
        /// <param name="baseClass">Obiekt który ma być zapisany</param>
        /// <param name="Location">Lokalizacja wskazana do zapisu</param>
        /// <param name="SpecifiedLocation">Nazwa pliku</param>
        /// <param name="TargetFileName"></param>
        /// 
        public void Save(object baseClass, string TargetLocalization, string TargetFileName)
        {

            try
            {
                CheckIfDirectoryExists(TargetLocalization);
                Save(baseClass, TargetLocalization + "\\" + TargetFileName);

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>
        /// Zapisuje obiekt jako plik xml.
        /// </summary>
        /// <param name="SavedObject">Obiekt do zapisania</param>
        /// <param name="filePath">Kompletna ścieżka zapisu</param>
        public void Save(object SavedObject, string filePath)
        {

            try
            {
                XmlSerializer serializer = new XmlSerializer(SavedObject.GetType());
                //CheckIfDirectoryExists(TargetLocalization);
                System.IO.FileStream file = System.IO.File.Create(filePath);
                serializer.Serialize(file, SavedObject);
                file.Close();

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>
        /// Zwraca zserializowany obiekt ze wskazanej lokalizacji (katalog + nazwa pliku)
        /// </summary>
        /// <param name="baseClass">Obiekt klasy docelowej, zserializowany obiekt będzie reprezentantem tej właśnie klasy.</param>
        /// <param name="SourceLocation">Katalog źródłowy</param>
        /// <param name="SourceFileName">Nazwa pliku</param>
        /// <returns></returns>
        public object Load(object baseClass, string SourceLocation, string SourceFileName)
        {
            try
            {
                if (SourceFileName.Length > 0 && SourceLocation.Length > 0)
                {
                    return Load(baseClass, SourceLocation + "\\" + SourceFileName);
                }
                else return null;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }

        /// <summary>
        /// Zwraca zserializowany obiekt ze wskazanej lokalizacji 
        /// </summary>
        /// <param name="baseClass">Obiekt klasy docelowej, zserializowany obiekt będzie reprezentantem tej właśnie klasy.</param>
        /// <param name="filePath">Ścieżka do pliku</param>
        /// <returns></returns>
        public virtual object Load(object baseClass, string filePath)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(baseClass.GetType());
                System.IO.TextReader tr = System.IO.File.OpenText(filePath);
                object result = serializer.Deserialize(tr);
                tr.Close();
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Zwraca zserializowany obiekt ze wskazanej lokalizacji 
        /// </summary>
        /// <typeparam name="T">Klasa której instancją będzie obiekt zwracany (serializacja przebiegnie pod kątem tej klasy)</typeparam>
        /// <param name="filePath">Pełna ścieżka do pliku</param>
        /// <returns></returns>
        public  object Load<T>( string filePath) where T:new()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                System.IO.TextReader tr = System.IO.File.OpenText(filePath);
                object result = serializer.Deserialize(tr);
                tr.Close();
                return result;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        private void CheckIfDirectoryExists(string TargetLocalization)
        {
            if (!System.IO.Directory.Exists(TargetLocalization))
            {
                System.IO.Directory.CreateDirectory(TargetLocalization);
            }
        }



    }
}
