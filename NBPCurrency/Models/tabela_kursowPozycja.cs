﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBPCurrency.Models
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class tabela_kursowPozycja
    {

        private string nazwa_walutyField;

        private ushort przelicznikField;

        private string kod_walutyField;

        private string kurs_sredniField;

        private string[] textField;

        /// <remarks/>
        public string nazwa_waluty
        {
            get
            {
                return this.nazwa_walutyField;
            }
            set
            {
                this.nazwa_walutyField = value;
            }
        }

        /// <remarks/>
        public ushort przelicznik
        {
            get
            {
                return this.przelicznikField;
            }
            set
            {
                this.przelicznikField = value;
            }
        }

        /// <remarks/>
        public string kod_waluty
        {
            get
            {
                return this.kod_walutyField;
            }
            set
            {
                this.kod_walutyField = value;
            }
        }

        /// <remarks/>
        public string kurs_sredni
        {
            get
            {
                return this.kurs_sredniField;
            }
            set
            {
                this.kurs_sredniField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string[] Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }
    }
}
