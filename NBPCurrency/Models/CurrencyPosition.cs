﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBPCurrency.Models
{
    public class CurrencyPosition
    {
        public string Code { get; set; }
        public double Rate { get; set; }
        public string Name { get; set; }
        public int Conversion { get; set; }
        public string Text { get; set; }

    }
}
