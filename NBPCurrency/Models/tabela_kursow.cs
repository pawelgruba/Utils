﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBPCurrency.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class tabela_kursow
    {

        private string numer_tabeliField;

        private System.DateTime data_publikacjiField;

        private tabela_kursowPozycja[] pozycjaField;

        private string typField;

        private string uidField;

        /// <remarks/>
        public string numer_tabeli
        {
            get
            {
                return this.numer_tabeliField;
            }
            set
            {
                this.numer_tabeliField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime data_publikacji
        {
            get
            {
                return this.data_publikacjiField;
            }
            set
            {
                this.data_publikacjiField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("pozycja")]
        public tabela_kursowPozycja[] pozycja
        {
            get
            {
                return this.pozycjaField;
            }
            set
            {
                this.pozycjaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string typ
        {
            get
            {
                return this.typField;
            }
            set
            {
                this.typField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uid
        {
            get
            {
                return this.uidField;
            }
            set
            {
                this.uidField = value;
            }
        }
    }
}
