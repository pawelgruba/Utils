﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBPCurrency
{
    public static class TableType
    {
        /// <summary>
        /// Definiuje typ zwracanej tabeli
        /// a - tabela kursów średnich walut obcych
        /// b - tabela kursów średnich walut niewymienialnych
        /// c - tabela kursów kupna i sprzedaży
        /// h - tabela kursów jednostek rozliczeniowych
        /// </summary>
        public enum CurrencyTableType { Tabela_A, Tabela_B, Tabela_C, Tabela_H };
    }
}
