﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace NBPCurrency.Models
{
    public class CurrencyTable
    {
        public DateTime PublicationDate { get; set; }
        public string TableNumber { get; set; }
        public List<NBPCurrency.Models.CurrencyPosition> Positions { get; set; }
        public string Type { get; set; }
        public string Uid { get; set; }
    }
}
