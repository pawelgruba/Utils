﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;

namespace NBPCurrency
{

    public class CurrencyNBP
    {


        /// <summary>
        /// Tworzy obiekt zawierający tabelę z kursami
        /// </summary>
        /// <param name="TableType">Definicja typu tabeli jaki ma być pobrany</param>
        /// <param name="TableDate">Określenie daty z której kursy mają zostać pobrane</param>
        public Models.CurrencyTable GetCurrency(NBPCurrency.TableType.CurrencyTableType TableType, DateTime TableDate)
        {
            try
            {
                List<string> listOfFiles = GetFilesList(@"http://www.nbp.pl/kursy/xml/dir.txt");
                string template = TableDate.ToString("yyMMdd");
                string actualFileName = listOfFiles.Where(q => q.Contains(template)).Where(w => w[0] == GetTableType(TableType)).FirstOrDefault();
                if (actualFileName == null || actualFileName.Length < 1) actualFileName = listOfFiles.Where(q => q[0] == 'a').LastOrDefault();
                string xmlFullPath = @"http://www.nbp.pl/kursy/xml/" + actualFileName + ".xml";
                DeserializeTable(xmlFullPath);
                return Currency = ConvertObject(TabelaKursow);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        internal NBPCurrency.Models.tabela_kursow TabelaKursow;

        private NBPCurrency.Models.CurrencyTable Currency;

        private char GetTableType(NBPCurrency.TableType.CurrencyTableType TableType)
        {
            switch (TableType)
            {
                case NBPCurrency.TableType.CurrencyTableType.Tabela_A: return 'a';
                case NBPCurrency.TableType.CurrencyTableType.Tabela_B: return 'b';
                case NBPCurrency.TableType.CurrencyTableType.Tabela_C: return 'c';
                case NBPCurrency.TableType.CurrencyTableType.Tabela_H: return 'h';
                default: return 'a';
            }
        }

        /// <summary>
        /// Konwertuje oryginalny obiekt na docelowy
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dest"></param>
        private NBPCurrency.Models.CurrencyTable ConvertObject(NBPCurrency.Models.tabela_kursow src)
        {
            try
            {
                NBPCurrency.Models.CurrencyTable dest = new Models.CurrencyTable();
                dest.PublicationDate = src.data_publikacji;
                dest.Type = src.typ;
                dest.Uid = src.uid;
                if (dest.Positions == null) dest.Positions = new List<NBPCurrency.Models.CurrencyPosition>();
                foreach (var item in src.pozycja)
                {
                    NBPCurrency.Models.CurrencyPosition cp = new Models.CurrencyPosition();

                    cp.Code = item.kod_waluty;
                    cp.Conversion = Convert.ToInt16(item.przelicznik);
                    cp.Name = item.nazwa_waluty;
                    cp.Text = item.Text == null ? string.Empty : item.Text.ToString();
                    //string test = item.kurs_sredni.Replace(',', '.');
                    cp.Rate = Convert.ToDouble(item.kurs_sredni);
                    dest.Positions.Add(cp);

                }
                return dest;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        private void DeserializeTable(string xmlFullPath)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(NBPCurrency.Models.tabela_kursow));
                XmlReader xreader = XmlReader.Create(GetStream(xmlFullPath));
                NBPCurrency.Models.tabela_kursow tk = (NBPCurrency.Models.tabela_kursow)serializer.Deserialize(xreader);
                TabelaKursow = tk;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private Stream GetStream(string xmlFullPath)
        {
            try
            {
                WebRequest wr = WebRequest.Create(xmlFullPath);
                WebResponse wrs = wr.GetResponse();
                Stream st = wrs.GetResponseStream();
                return st;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Pobiera listę plików xml dostępnych na stronach NBP
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private List<string> GetFilesList(string url)
        {
            try
            {
                List<string> fileLists = new List<string>();

                if (url != null && url.Length > 0)
                {
                    StreamReader sr = new StreamReader(GetStream(url));
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        fileLists.Add(line);
                    }
                }

                return fileLists;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }








    }




}
