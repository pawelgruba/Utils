﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Generators;
using Generators.Enums;

namespace Utils.Test
{
    [TestClass]
    public class Generators
    {
        private int numberOfTests = 100;
        private string result;
        private Pesel pesel = new Pesel();

        [TestMethod]
        public void TestPesel()
        {
            for (int i = 0; i < numberOfTests; i++)
            {
                Assert.IsTrue(pesel.Validate(
                    pesel.Generate(
                        DateTime.Now.AddMonths(i * (-1)))));
            }
        }

        [TestMethod]
        public void FemailPesel()
        {
            for (int i = 0; i < numberOfTests; i++)
            {
                result = pesel.Generate(Gender.Female);

                Assert.IsTrue(pesel.Validate(result));
                Assert.IsTrue(pesel.IsFemale(result));
            }
        }

        [TestMethod]
        public void MailPesel()
        {
            for (int i = 0; i < numberOfTests; i++)
            {
                result = pesel.Generate(Gender.Male);

                Assert.IsTrue(pesel.Validate(result));
                Assert.IsTrue(pesel.IsMale(result));
            }
        }

        [TestMethod]
        public void DateRangePesel()
        {
            for (int i = 0; i < numberOfTests; i++)
            {
                result = pesel.Generate(60, 5);
            }
        }
    }
}
