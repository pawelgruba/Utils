﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensions
{
    public static class StringEXT
    {
        /// <summary>
        /// Trim string tu closest space at index position.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        public static string TrimToIndex(this string input, int index, string suffix)
        {
            if (input.Length > 0 &&
                input.Contains(" ") &&
                (index > 0) &&
                (index < input.Length))
            {
                int SPACE_CHAR_CODE = 32;
                int closestSpace = 0;
                for (int i = 0; i < index; i++)
                {
                    if (input[i] == ((char)SPACE_CHAR_CODE))
                    {
                        closestSpace = i;
                    }
                }

                return input.Substring(0, closestSpace) + suffix;
            }
            else if (input.Length + suffix.Length <= index && input.Length > 0)
            {
                return input + suffix;
            }
            else
            {
                return input;
            }
        }

        /// <summary>
        /// Neeed improve for very small maxRowLength
        /// </summary>
        /// <param name="input"></param>
        /// <param name="maxRowLength"></param>
        /// <returns></returns>
        public static string[] ToBlock(this string input, int maxRowLength)
        {
            if (input.Length > 0 &&
                input.Contains(" ") &&
                (maxRowLength > 0) &&
                (maxRowLength < input.Length))
            {
                List<string> result = new List<string>();
                int SPACE_CHAR_CODE = 32;
                int rowIterator = -1;
                int closestSpace = 0;
                int lastIndex = 0;

                for (int i = 0; i <= input.Length; i++)
                {
                    rowIterator++;
                    if (i < input.Length)
                    {

                        if (rowIterator <= maxRowLength)
                        {
                            if (input[i] == ((char)SPACE_CHAR_CODE))
                            {
                                closestSpace = i;
                            }
                        }
                        else
                        {
                            result.Add(input.Substring(lastIndex, (closestSpace-lastIndex)).TrimStart());
                            lastIndex = closestSpace;
                            rowIterator = -1;
                        }
                    }
                    else
                    {
                        result.Add(input.Substring(lastIndex, input.Length-lastIndex).TrimStart());
                        break;
                    }

                }

                return result.ToArray();
            }
            else if (input.Length <= maxRowLength)
            {
                return new string[1] { input };
            }

            return new string[0];
        }

        private static string GetNextRow(string input, int lastIndex, int charCode)
        {
            int initSpace = lastIndex;
            int lastSpace = 0;
            for (int i = lastIndex; i < input.Length; i++)
            {
                if (input[i] == ((char)charCode))
                {
                    lastSpace = i;
                }


            }

            string result = input.Substring(initSpace, lastSpace);
            lastIndex = lastSpace;
            input = result;
            return result;
        }
    }
}
