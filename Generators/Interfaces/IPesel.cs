﻿using Generators.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generators.Interfaces
{
    public interface IPesel
    {
        string Generate(DateTime BirthDate, Gender Gender);

        string Generate(Gender Gender);

        string Generate(DateTime BirthDate);

        string Generate(int MinimumAge);

        string Generate(int MinimumAge, Gender Gender);

        string Generate(int MaximumAge, int MinimumAge);

        string Generate(int MaximumAge, int MinimumAge, Gender Gender);

        bool IsFemale(string pesel);

        bool IsMale(string pesel);
    }
}
