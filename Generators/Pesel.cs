﻿using System;
using Generators.Enums;
using Generators.Interfaces;

namespace Generators
{
    public class Pesel : IPesel
    {
        private Random random = new Random();
        private int[] _female = new int[] { 0, 2, 4, 6, 8 };
        private int[] _male = new int[] { 1, 3, 5, 7, 9 };
        public bool Validate(string Pesel)
        {
            if (Pesel.Length == 11)
            {
                int reszta = controlSum(Pesel);

                if (int.Parse(Pesel[Pesel.Length - 1].ToString()) == reszta)
                {
                    return true;
                }
                else
                {
                    if (reszta == (10 - int.Parse(Pesel[Pesel.Length - 1].ToString())) || reszta == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }



        private int controlSum(string Pesel)
        {
            int sum = 0;
            string toValidate = Pesel.Length > 10 ? Pesel.Substring(0, 10) : Pesel;
            for (int i = 0; i <= toValidate.Length; i++)
            {
                switch (i+1)
                {
                    case 1:
                    case 5:
                    case 9: sum += (int.Parse(Pesel[i].ToString())); break;
                    case 2:
                    case 6:
                    case 10: sum += (int.Parse(Pesel[i].ToString()) * 3); break;
                    case 3:
                    case 7: sum += (int.Parse(Pesel[i].ToString()) * 7); break;
                    case 4:
                    case 8: sum += (int.Parse(Pesel[i].ToString()) * 9); break;
                }
            }

            int result =  sum % 10;
            return result == 0 ? 0 : (10 - result);
        }

        public string Generate(DateTime BirthDate)
        {
            return Generate(BirthDate, (Enums.Gender)random.Next(0, 2));
        }

        

        public string Generate(DateTime BirthDate,Gender gender)
        {
            string result = string.Empty;

            int addToMonth = 0;

            if (BirthDate.Year < 1900)
                addToMonth += 80;
            else if (BirthDate.Year < 2000)
                addToMonth = 0;
            else if (BirthDate.Year < 2100)
                addToMonth += 40;
            else if (BirthDate.Year < 2300)
                addToMonth += 60;

            result += (int.Parse(((BirthDate.Year).ToString().Substring(2, 2))) + addToMonth).ToString();
            result += BirthDate.Month.ToString().PadLeft(2, '0');
            result += BirthDate.Day.ToString().PadLeft(2, '0');
            //zapychacze
            result += new Random().Next(100,1000);
            //gender
            switch ((int)gender) {
                case 0: result += _male[random.Next(0, _male.Length)];
                    break;
                case 1: result += _female[random.Next(0, _female.Length)];
                    break;
            }

            result += controlSum(result);

            return result;
        }

        /// <summary>
        /// Generate random pesel for specific gender. Max age = 100 years.
        /// </summary>
        /// <param name="Gender"></param>
        /// <returns></returns>
        public string Generate(Gender Gender)
        {
            return Generate(DateTime.Now.AddDays(-(random.Next(1,365*100))), Gender);
        }

        /// <summary>
        /// Generate Pesel for person who is at least 'MinimumAge' old.
        /// </summary>
        /// <param name="MinimumAge"></param>
        /// <returns></returns>
        public string Generate(int MinimumAge)
        {
            return Generate(DateTime.Now.AddYears(-MinimumAge));
        }

        /// <summary>
        /// Generate pesel for definied gender and 
        /// </summary>
        /// <param name="MinimumAge"></param>
        /// <param name="Gender"></param>
        /// <returns></returns>
        public string Generate(int MinimumAge, Gender Gender)
        {
            return Generate(DateTime.Now.AddDays(-(random.Next(MinimumAge*365,365*100))), Gender);
        }

        /// <summary>
        /// Generate pesel for person in age range.
        /// </summary>
        /// <param name="MaximumAge"></param>
        /// <param name="MinimumAge"></param>
        /// <returns></returns>
        public string Generate(int MaximumAge, int MinimumAge)
        {
            return Generate(DateTime.Now.AddDays(-(random.Next(MinimumAge * 365, MaximumAge * 365))));
        }

        /// <summary>
        /// Generate pesel for person in age range and specified gender.
        /// </summary>
        /// <param name="MaximumAge">Maximum age for pesel</param>
        /// <param name="MinimumAge">Minimum age for pesel</param>
        /// <param name="Gender">Gender</param>
        /// <returns></returns>
        public string Generate(int MaximumAge, int MinimumAge, Gender Gender)
        {
            return Generate(DateTime.Now.AddDays(-(random.Next(MinimumAge * 365, MaximumAge * 365))),Gender);

        }

        public bool IsMale(string pesel)
        {
            int genderValue = int.Parse(pesel[pesel.Length - 2].ToString());
            foreach (var item in _male)
            {
                if (item == genderValue)
                    return true;
            }

            return false;
        }

        public bool IsFemale(string result)
        {
            int genderValue = int.Parse(result[result.Length - 2].ToString());
            foreach (var item in _female)
            {
                if (item == genderValue)
                    return true;
            }

            return false;
        }
    }
}
