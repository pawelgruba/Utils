﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace GoogleDistanceAPI
{
    public class GoogleDistanceAPI
    {
        /// <summary>
        /// Tworzy instancję klasy GoogleDistanceAPI
        /// </summary>
        /// <param name="googleKey">Google API Key</param>
        public GoogleDistanceAPI(string googleKey)
        {
            this.googleKey = googleKey;
        }

        /// <summary>
        /// Tworzy instancję klasy GoogleDistanceAPI
        /// </summary>
        /// <param name="googleKey">Google API Key</param>
        /// <param name="language">Wybór języka w jakim zwracana będzie odpowiedź</param>
        public GoogleDistanceAPI(string googleKey, Model.Enums.Language language)
        {
            this.googleKey = googleKey;
            this.language = language.ToString();
        }

        /// <summary>
        /// Tworzy instancję klasy GoogleDistanceAPI
        /// </summary>
        /// <param name="googleKey">Google API Key</param>
        /// <param name="language">Wybór języka w jakim zwracana będzie odpowiedź</param>
        /// <param name="units">Wybór jednostek w jakich zwrócona zostanie odpowiedź</param>
        public GoogleDistanceAPI(string googleKey, Model.Enums.Language language, Model.Enums.Units units)
        {
            this.googleKey = googleKey;
            this.language = language.ToString();
            this.units = units.ToString();
        }

        /// <summary>
        /// Tworzy instancję klasy GoogleDistanceAPI
        /// </summary>
        /// <param name="googleKey">Google API Key</param>
        /// <param name="language">Wybór języka w jakim zwracana będzie odpowiedź</param>
        /// <param name="units">Wybór jednostek w jakich zwrócona zostanie odpowiedź</param>
        /// <param name="DrivingMode">Wybór sposobu podróżowania dla jakiego liczona będą wartości</param>
        public GoogleDistanceAPI(string googleKey, Model.Enums.Language language, Model.Enums.Units units, Model.Enums.DrivingMode DrivingMode)
        {
            this.googleKey = googleKey;
            this.language = language.ToString();
            this.units = units.ToString();
            this.mode = DrivingMode.ToString();
        }

        private const string _apiXmlAddress = @"https://maps.googleapis.com/maps/api/distancematrix/xml?origins={0}&destinations={1}&key={2}&mode={3}&sensor=false&language={4}&units={5}";

        private const string _apiJsonAddress = @"https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&key={2}&mode={3}&sensor=false&language={4}&units={5}";

        private string googleKey { get; set; }

        private string origins { get; set; }

        private string destinations { get; set; }

        private string language = "en";

        private string units = "metric";

        private string mode = "driving";

        public Model.DistanceMatrixResponse GetCalculation()
        {
            if (origins.Length > 0 && destinations.Length > 0)
            {
                Model.DistanceMatrixResponse result = new Model.DistanceMatrixResponse();
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(_apiXmlAddress, origins, destinations, googleKey,mode, language,units));
                    WebResponse response = request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    XmlSerializer serializer = new XmlSerializer(typeof(Model.DistanceMatrixResponse));
                    result = (Model.DistanceMatrixResponse)serializer.Deserialize(dataStream);
                    return result;
                }
                catch (Exception e)
                {

                    throw e;
                }


            }
            else return null;
        }

        /// <summary>
        /// Metoda do ustawienia lokalizacji źródłowych
        /// </summary>
        /// <param name="origins">Lista adresów źródłowych</param>
        public void SetOrigins(List<string> origins)
        {
            try
            {
                if (origins.Count > 0)
                {
                    this.origins = origins.Aggregate((s1, s2) => s1 + "|" + s2);
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        /// <summary>
        /// Metoda do ustawiania lokalizacji docelowych
        /// </summary>
        /// <param name="destinations">Lista adresów docelowych</param>
        public void SetDestinations(List<string> destinations)
        {
            try
            {
                if (destinations.Count > 0)
                {
                    this.destinations = destinations.Aggregate((s1, s2) => s1 + "|" + s2);
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
    }
}
