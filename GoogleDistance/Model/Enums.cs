﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoogleDistanceAPI.Model
{
    public class Enums
    {
        public enum DrivingMode { driving, walking, bicycling, transit }

        public enum Units { metric, imperial }

        public enum Avoids { tolls, highways, ferries, indoor }

        public enum Language { pl, en }
    }
}
