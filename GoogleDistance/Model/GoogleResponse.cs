﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GoogleDistanceAPI.Model
{

    [XmlRoot]
    public class DistanceMatrixResponse
    {
        [XmlElement("status")]
        public string Status { get; set; }

        [XmlElement("origin_address")]
        public List<string> OriginAddress = new List<string>();

        [XmlElement("destination_address")]
        public List<string> DestinationAddress = new List<string>();

        [XmlElement("row")]
        public List<Row> Results = new List<Row>();

        public class Duration
        {
            [XmlElement("value")]
            public int Value { get; set; }

            [XmlElement("text")]
            public string Text { get; set; }
        }

        public class Distance
        {
            [XmlElement("value")]
            public int Value { get; set; }

            [XmlElement("text")]
            public string Text { get; set; }
        }

        [XmlRoot]
        public class Result
        {
            [XmlElement("status")]
            public string Status { get; set; }

            [XmlElement("duration")]
            public Duration Duration = new Duration();

            [XmlElement("distance")]
            public Distance Distance = new Distance();
        }

        public class Row
        {
            [XmlElement("element")]
            public List<Result> Element = new List<Result>();
        }
    }


}
