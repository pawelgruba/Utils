﻿using System;
using System.Management;
using System.Text;
namespace SystemUtil
{
    public class Processor
    {
        private ManagementClass mc;
        private ManagementObjectCollection moc;
        private ManagementObject _currentObject;
        public Processor()
        {
            //todo: some properties can rise exceptions because their values are null in properties collection! check its values before return!
            mc = new ManagementClass("win32_processor");
            moc = mc.GetInstances();
            if (moc.Count > 0)
            {
                foreach (var item in moc)
                {
                    _currentObject = item as ManagementObject;
                    break;
                }
            }
        }
        StringBuilder sb = new StringBuilder();

        public UInt16 AddressWidth
        {

            get { return ushort.Parse(_currentObject.Properties["AddressWidth"].Value.ToString()); }
        }

        public UInt16 Architecture
        {

            get { return ushort.Parse(_currentObject.Properties["Architecture"].Value.ToString()); }
        }

        public String AssetTag
        {
            get { return _currentObject.Properties["AssetTag"].Value == null ? string.Empty : _currentObject.Properties["AssetTag"].Value.ToString(); }
        }
        public UInt16 Availability
        {

            get { return ushort.Parse(_currentObject.Properties["Availability"].Value.ToString()); }
        }
        public String Caption
        {
            get { return _currentObject.Properties["Caption"].Value == null ? string.Empty : _currentObject.Properties["Caption"].Value.ToString(); }
        }
        public uint Characteristics
        {
            get { return ushort.Parse(_currentObject.Properties["Characteristics"].Value.ToString()); }
        }
        public uint? ConfigManagerErrorCode
        {
            get { return _currentObject.Properties["ConfigManagerErrorCode"].Value == null ? uint.MinValue : ushort.Parse(_currentObject.Properties["ConfigManagerErrorCode"].Value.ToString());
            }
        }
        public Boolean ConfigManagerUserConfig
        {
            get { return Boolean.Parse(_currentObject.Properties["ConfigManagerUserConfig"].Value.ToString()); }
        }
        public UInt16 CpuStatus
        {
            get { return ushort.Parse(_currentObject.Properties["CpuStatus"].Value.ToString()); }
        }
        public String CreationClassName
        {
            get { return _currentObject.Properties["CreationClassName"].Value == null ? string.Empty : _currentObject.Properties["CreationClassName"].Value.ToString(); }
        }
        public UInt32 CurrentClockSpeed
        {
            get { return ushort.Parse(_currentObject.Properties["CurrentClockSpeed"].Value.ToString()); }
        }
        public UInt16 CurrentVoltage
        {
            get { return ushort.Parse(_currentObject.Properties["CurrentVoltage"].Value.ToString()); }
        }
        public UInt16 DataWidth
        {
            get { return ushort.Parse(_currentObject.Properties["DataWidth"].Value.ToString()); }
        }
        public String Description
        {
            get { return _currentObject.Properties["Description"].Value == null ? string.Empty : _currentObject.Properties["Description"].Value.ToString(); }
        }
        public String DeviceID
        {
            get { return _currentObject.Properties["DeviceID"].Value == null ? string.Empty : _currentObject.Properties["DeviceID"].Value.ToString(); }
        }
        public Boolean ErrorCleared
        {
            get { return Boolean.Parse(_currentObject.Properties["ErrorCleared"].Value.ToString()); }
        }
        public String ErrorDescription
        {
            get { return _currentObject.Properties["ErrorDescription"].Value == null ? string.Empty : _currentObject.Properties["ErrorDescription"].Value.ToString(); }
        }
        public UInt32 ExtClock
        {
            get { return ushort.Parse(_currentObject.Properties["ExtClock"].Value.ToString()); }
        }
        public UInt16 Family
        {
            get { return ushort.Parse(_currentObject.Properties["Family"].Value.ToString()); }
        }
        public DateTime InstallDate
        {
            get { return DateTime.Parse(_currentObject.Properties["InstallDate"].Value.ToString()); }
        }
        public UInt32 L2CacheSize
        {
            get { return ushort.Parse(_currentObject.Properties["L2CacheSize"].Value.ToString()); }
        }
        public UInt32 L2CacheSpeed
        {
            get { return ushort.Parse(_currentObject.Properties["L2CacheSpeed"].Value.ToString()); }
        }
        public UInt32 L3CacheSize
        {
            get { return ushort.Parse(_currentObject.Properties["L3CacheSize"].Value.ToString()); }
        }
        public UInt32 L3CacheSpeed
        {
            get { return ushort.Parse(_currentObject.Properties["L3CacheSpeed"].Value.ToString()); }
        }
        public UInt32 LastErrorCode
        {
            get { return ushort.Parse(_currentObject.Properties["LastErrorCode"].Value.ToString()); }
        }
        public UInt16 Level
        {
            get { return ushort.Parse(_currentObject.Properties["Level"].Value.ToString()); }
        }
        public UInt16 LoadPercentage
        {
            get { return ushort.Parse(_currentObject.Properties["LoadPercentage"].Value.ToString()); }
        }
        public String Manufacturer
        {
            get { return _currentObject.Properties["Manufacturer"].Value == null ? string.Empty : _currentObject.Properties["Manufacturer"].Value.ToString(); }
        }
        public UInt32 MaxClockSpeed
        {
            get { return ushort.Parse(_currentObject.Properties["MaxClockSpeed"].Value.ToString()); }
        }
        public String Name
        {
            get { return _currentObject.Properties["Name"].Value == null ? string.Empty : _currentObject.Properties["Name"].Value.ToString(); }
        }
        public UInt32 NumberOfCores
        {
            get { return ushort.Parse(_currentObject.Properties["NumberOfCores"].Value.ToString()); }
        }
        public UInt32 NumberOfEnabledCore
        {
            get { return ushort.Parse(_currentObject.Properties["NumberOfEnabledCore"].Value.ToString()); }
        }
        public UInt32 NumberOfLogicalProcessors
        {
            get { return ushort.Parse(_currentObject.Properties["NumberOfLogicalProcessors"].Value.ToString()); }
        }
        public String OtherFamilyDescription
        {
            get { return _currentObject.Properties["OtherFamilyDescription"].Value == null ? string.Empty : _currentObject.Properties["OtherFamilyDescription"].Value.ToString(); }
        }
        public String PartNumber
        {
            get { return _currentObject.Properties["PartNumber"].Value.ToString(); }
        }
        public String PNPDeviceID
        {
            get { return _currentObject.Properties["PNPDeviceID"].Value == null ? string.Empty : _currentObject.Properties["PNPDeviceID"].Value.ToString(); }
        }
        public UInt16 PowerManagementCapabilities
        {
            get { return ushort.Parse(_currentObject.Properties["PowerManagementCapabilities"].Value.ToString()); }
        }
        public Boolean PowerManagementSupported
        {
            get { return Boolean.Parse(_currentObject.Properties["PowerManagementSupported"].Value.ToString()); }
        }
        public String ProcessorId
        {
            get { return _currentObject.Properties["ProcessorId"].Value == null ? string.Empty : _currentObject.Properties["ProcessorId"].Value.ToString(); }
        }
        public UInt16 ProcessorType
        {
            get { return ushort.Parse(_currentObject.Properties["ProcessorType"].Value.ToString()); }
        }
        public UInt16 Revision
        {
            get { return ushort.Parse(_currentObject.Properties["Revision"].Value.ToString()); }
        }
        public String Role
        {
            get { return _currentObject.Properties["Role"].Value == null ? string.Empty : _currentObject.Properties["Role"].Value.ToString(); }
        }
        public Boolean SecondLevelAddressTranslationExtensions
        {
            get { return Boolean.Parse(_currentObject.Properties["SecondLevelAddressTranslationExtensions"].Value.ToString()); }
        }
        public String SerialNumber
        {
            get { return _currentObject.Properties["SerialNumber"].Value == null ? string.Empty : _currentObject.Properties["SerialNumber"].Value.ToString(); }
        }
        public String SocketDesignation
        {
            get { return _currentObject.Properties["SocketDesignation"].Value == null ? string.Empty : _currentObject.Properties["SocketDesignation"].Value.ToString(); }
        }
        public String Status
        {
            get { return _currentObject.Properties["Status"].Value == null ? string.Empty : _currentObject.Properties["Status"].Value.ToString(); }
        }
        public UInt16 StatusInfo
        {
            get { return ushort.Parse(_currentObject.Properties["StatusInfo"].Value.ToString()); }
        }
        public String Stepping
        {
            get { return _currentObject.Properties["Stepping"].Value == null ? string.Empty : _currentObject.Properties["Stepping"].Value.ToString(); }
        }
        public String SystemCreationClassName
        {
            get { return _currentObject.Properties["SystemCreationClassName"].Value == null ? string.Empty : _currentObject.Properties["SystemCreationClassName"].Value.ToString(); }
        }
        public String SystemName
        {
            get { return _currentObject.Properties["SystemName"].Value == null ? string.Empty : _currentObject.Properties["SystemName"].Value.ToString(); }
        }
        public UInt32 ThreadCount
        {
            get { return ushort.Parse(_currentObject.Properties["ThreadCount"].Value.ToString()); }
        }
        public String UniqueId
        {
            get { return _currentObject.Properties["UniqueId"].Value == null ? string.Empty : _currentObject.Properties["UniqueId"].Value.ToString(); }
        }
        public UInt16 UpgradeMethod
        {
            get { return ushort.Parse(_currentObject.Properties["UpgradeMethod"].Value.ToString()); }
        }
        public String Version
        {
            get { return _currentObject.Properties["Version"].Value == null ? string.Empty : _currentObject.Properties["Version"].Value.ToString(); }
        }
        public Boolean VirtualizationFirmwareEnabled
        {
            get { return Boolean.Parse(_currentObject.Properties["VirtualizationFirmwareEnabled"].Value.ToString()); }
        }
        public Boolean VMMonitorModeExtensions
        {
            get { return Boolean.Parse(_currentObject.Properties["VMMonitorModeExtensions"].Value.ToString()); }
        }
        public UInt32 VoltageCaps
        {
            get { return ushort.Parse(_currentObject.Properties["VoltageCaps"].Value.ToString()); }
        }
    }
}

